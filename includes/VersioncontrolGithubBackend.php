<?php

class VersioncontrolGithubBackend extends VersioncontrolGitBackend {

  public $type = 'github';

  public $classesEntities =  [
    'repo' => 'VersioncontrolGithubRepository',
    'account' => 'VersioncontrolGitAccount',
    'operation' => 'VersioncontrolGitOperation',
    'item' => 'VersioncontrolGitItem',
    'event' => 'VersioncontrolGitEvent',
  ];

  public function __construct() {
    parent::__construct();
    $this->name = 'Github';
    $this->description = t('Integrates with Github throught github_api module.');
  }

  /**
   * Provide default plugins.
   */
  public function getDefaultPluginName($plugin_type) {
    // Use our reposync and repomgr provided plugin as default.
    if ($plugin_type == 'reposync') {
      return 'github';
    }
  }

}
