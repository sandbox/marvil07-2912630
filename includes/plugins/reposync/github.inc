<?php

$plugin = [
  'title' => t('Github history synchronizer'),
  'worker' => [
    'class' => 'VersioncontrolGithubRepositoryHistorySynchronizer',
  ],
];
