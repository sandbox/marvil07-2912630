# Version Control API -- Github integration

Provides integration between a subset of github API so it can integrate with
versioncontrol.

Currently using github_api module to do the integration, the underlying library
used is php-github-api.
